#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#
#	record.py
#
#  Copyright 2014 gumbicp <pracacp@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

"""
	Moduł nagrywania programem recordmydesktop. Wersja linux.
"""


__author__ = 'gumbicp'
__email__ = 'pracacp@gmail.com'
__version__ = '1.0.0'


from time import localtime, strftime		# dla lokalnego czasu
import argparse								# polecenia przysłane z terminalu
import subprocess							# dla komend
import sys									# dla  funkcji sys.exit()

##
# stałe domyślne
#
FREQ = 22050
FPS = 60
SCREEN_HEIGHT = 740
SCREEN_WIDTH = 1060
OFFSET_X = 0
OFFSET_Y = 0

class Recorder(object):
	"""
	Klasa wysyła komendę do powłoki systemowej. Start nagrywania.
	Wykorzystuję do tego metodę z modułu suprocess  call().
	Ustawia nazwę pliku z automatu na tut-czasLokalny.ogv.
	
	"""
	# domyślna nazwa pliku wyjsciowego dla recordmydesktop
	outname = "tut-{}.ogv".format(strftime("%a_%d_%b_%Y_%H:%M:%S", localtime()))
	# tablica opcji dla programu 
	full_command = ['recordmydesktop']
	
	def reset_full_command(self):
		"""
		Metoda ustawia tablice komend bez flag.
		"""
		self.full_command = ['recordmydesktop']
	
	def process(self, args):
		"""
		Metoda process ustawia wszystkie opcje i na koncu wysyła polecenie do wiersza
		poleceń metodą subprocess.call().
		
		@args: arugumenty parsera argparse
		
		"""		
		self.full_command.append("-x")
		self.full_command.append("{}".format(args.x))
		self.full_command.append("-y")
		self.full_command.append("{}".format(args.y))
		self.full_command.append("--width")
		self.full_command.append("{}".format(args.width))
		self.full_command.append("--height")
		self.full_command.append("{}".format(args.height))
		self.full_command.append("--fps")
		self.full_command.append("{}".format(args.fps))
		self.full_command.append('--stop-shortcut')
		self.full_command.append('Control+Mod1+s')
		#opcionalne
		if args.quiet:
			self.full_command.append("--no-sound")
		if args.no_cursor:
			self.full_command.append("--no-cursor")
		if args.device:
			self.full_command.append("--device")
			self.full_command.append("{}".format(args.device))
		if args.on_the_fly_encoding:
			self.full_command.append("--on-the-fly-encoding")
		if args.v_bitrate:
			self.full_command.append("--v_bitrate")
			self.full_command.append("{}".format(args.v_bitrate))
		if args.delay:
			self.full_command.append("--delay")
			self.full_command.append("{}".format(args.delay))
		if args.overwrite:
			self.full_command.append("--overwrite")
		if args.no_wm_check:
			self.full_command.append("--no-wm-check")
			
		self.full_command.append('--freq')
		self.full_command.append("{}".format(args.freq))	
		self.full_command.append("-o")
		self.full_command.append("{}".format(args.output))
		
		print(self.full_command)
		subprocess.call(self.full_command, stderr=subprocess.STDOUT)
		
	def one_command(self, command):
		"""
		Metoda dla jednej komendy. Po wykonaniu kończy program.
		
		@command: string
		"""
		self.reset_full_command()
		self.full_command.append(command)
		subprocess.call(self.full_command, stderr=subprocess.STDOUT)
		sys.exit(0)
		
	

def main():
	"""
	Funkcja parsera argparse lini poleceń przysłanych z powłoki systemowej sys.argv[]
	"""
	# obiekt klasy Recorder
	objr = Recorder()
	# parsel lini poleceń
	parser = argparse.ArgumentParser(description="Record My Desktop Python3 script. For Linux program recordmydesctop."
												 " Author python version gumbicp. Orginal Perl code write by PIOTAO"
												 " from PKB Polish Blender Tutorials http://polskikursblendera.pl/ "
												 "Tutorial: "
												 " http://polskikursblendera.pl/technologia-nagrywania-poradnikow-wideo/")
	# manual record my desktop
	parser.add_argument('--manual',
						default=False,
						action="store_true",
						help="recordmydesktop manual(full)"
						)
	# info o opcjach wykorzystanych przy kompilacji recordmydesktop
	parser.add_argument('--print-config',
						default=False,
						action="store_true",
						help="Print info about options selected during compilation and exit."
						)
	# nazwa pliku
	parser.add_argument('-o', '--output',
						default=objr.outname,
						help='OUTNAME -output file to create (default tut-localtime.ogv)'
						)
	# bez głosu
	parser.add_argument(
						'-q', '--quiet',
						default=False,
						action="store_true",
						help='--no-sound  Do not record sound.'
						)
	# bez głosu
	parser.add_argument(
						'--no-wm-check',
						default=False,
						action="store_true"
						)
	# oś x na ekranie
	parser.add_argument(
						'-x', '--x',
						default=OFFSET_X, metavar='int', type=int,
						help='Offset in x direction.'
						)
	# oś y na ekranie
	parser.add_argument(
						'-y', '--y',
						default=OFFSET_Y, metavar='int', type=int,
						help='Offset in y direction.'
						)
	# szerokość ekranu nagrywania
	parser.add_argument('--width',
						default=SCREEN_WIDTH, metavar='int', type=int,
						help='Width of recorded window.'
						)
	# wysokość ekranu nagrywania
	parser.add_argument('--height',
						default=SCREEN_HEIGHT, metavar='int', type=int,
						help='Height of recorded window.'
						)
	# klatki na sekundę
	parser.add_argument('--fps',
						default=FPS, metavar='int', type=int,
						help='A positive number denoting desired framerate.'
						)
	# wyłącz kursor
	parser.add_argument('--no-cursor',
						default=False,
						action="store_true",
						help='Disable drawing of the cursor'
						)
	# urządzenie dźwięk
	parser.add_argument('--device',
						default=False, metavar='hw:0,0',
						help='Sound device(default hw:0,0 or /dev/dsp, depending on whether ALSA or OSS is used).'
						)
	# częstotliwość
	parser.add_argument('--freq',
						default=FREQ, metavar='int', type=int,
						help="N(number>0) A positive number denoting desired sound frequency. Default 22050Hz."
						)
	# kodowanie w locie
	parser.add_argument('--on-the-fly-encoding',
						default=False,
						action="store_true",
						help="Encode the audio-video data, while recording."
						)
	# kodowanie bitrate
	parser.add_argument('--v_bitrate',
						default=False, metavar='int', type=int,
						help="A number from 45000 to 2000000 for desired encoded video bitrate(default 45000)."
						)
	# opuźnienie nagrywania
	parser.add_argument('--delay',
						default=False, metavar='int/float',
						help="n[H|h|M|m] Number of secs(default),minutes or hours before capture"
						" starts(number can be float)."
						)
	parser.add_argument('--overwrite',
						default=False,
						action="store_true",
						help="If there is already a file with the same name, delete it."
						"Default action is to add a number postfix to the new file."
						"For example when not specifying a name, if out.ogv exists,"
						"the new file will be out-1.ogv and if that exists too, out-2.ogv"
						"and so on (no ad-infinitum though, more like ad-short-integer...)"
						)
	# argumenty parsera
	args = parser.parse_args()
	
	# opcja --manual
	if args.manual:
		#cały manual recordmydesktop wypisany w terminalu
		proc = subprocess.Popen(['man', 'recordmydesktop'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
		stdout_value = proc.communicate(b'/t return man recordmydesktop')[0]
		print(stdout_value.decode())
		sys.exit(0)
	# dane kompilacji
	elif args.print_config:
		objr.one_command("--print-config")
		
		
	# obiekt klasy Recorder
	objr.process(args)


##
#	start programu ./record.py
#
if __name__ == '__main__':
	main()
