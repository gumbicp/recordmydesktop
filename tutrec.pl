#!/usr/bin/perl -w
use strict;
use POSIX qw{strftime};
use Getopt::Long;
use Data::Dumper;

my %CFG = (
  outname => strftime("tut-%Y%m%d:%H%M.ogv",localtime),
  silent => 0,
  record => 'recordmydesktop',
  window => '',
  fullscr => '',
  freq => 22050,
  nadpisz => 1,
  delay => 2,
  tmpdir => '.tmp',
  onfly => 0,
  adevice => 'hw:0,0',
);
GetOptions(
 # -o, --outname, --output NAME -output file to create (default tut-datetime.ogv)
 'outname|o:s' => $CFG{outname},

 # -q, --na, --noaudio, --silent -no audio record (default with audio)
 'silent|noaudio|na|q!' => $CFG{silent},

 'fullscreen|f!' => $CFG{fullscr},
 # -f, --fullscreen, --full -record full screen (will ignore -w)

 # -w, --window HEX -window id number (will ask if no -f)
 'window|w:i' => $CFG{window},

 # --freq INT -sound sampling rate (defualt 22050)
 'frequency|freq:i' => $CFG{freq},

 # --over -always overwrite files (default)
 'overwrite|over!' => $CFG{nadpisz},

 # -d, --delay INT -wait INT seconds before start (default 2)
 'delay|d' => $CFG{delay},

 # -t /tmp -temporary dir for intermediate files (choose fast filesystem)
 'tempdir|tmpdir|tmp|t:s' => $CFG{tmpdir},

 # --of -enable on the fly recording
 'onthefly|onfly|of|e!' => $CFG{onfly},

 # -a, --adevice NAME -audio recording device (i.e. hw:1,1 for 2nd audigy 2nd mic)
 'adevice|a:s' => $CFG{adevice},

 # just this help
 'help|h!' => &help,

);
$CFG{window} = getWindowID() if !$CFG{fullscr} and !$CFG{window};
my $cmd = '{record} -o {outname} --channels 1 --delay {delay} --workdir {tmpdir}';
$cmd .= $CFG{silent} ? " --no-sound" : " --freq {freq} --device {adevice}";
$cmd .= " --overwrite" if $CFG{nadpisz};
$cmd .= " --on-the-fly-encoding" if $CFG{onfly};
$cmd .= $CFG{fullscr} ? "" : $CFG{window} ? " --windowid {window} " : "";
while( $cmd =~ m#{(w+)}#o ){
 my $key = $1;
 print "key=$keyn";
 $cmd =~ s/{$key}/$CFG{$key}/g;
}
if( !-d $CFG{tmpdir}){
  mkdir $CFG{tmpdir},0755
    or warn "$0: can't make temp dir [$CFG{tmpdir}]; $!n";
}
print $cmd;
system $cmd;
exit;
sub getWindowID {
 my $windowid = (split /s+/,(grep /Window id:/, `xwininfo`)[0])[3];
}
sub help {
 print <<EOT;
 -o, --outname, --output NAME -output file to create (default tut-datetime.ogv)
 -q, --na, --noaudio, --silent -no audio record (default with audio)
 -f, --fullscreen, --full -record full screen (will ignore -w)
 -w, --window HEX -window id number (will ask if no -f)
 --freq INT -sound sampling rate (defualt 22050)
 --over -always overwrite files (default)
 -d, --delay INT -wait INT seconds before start (default 2)
 -t /tmp -temporary dir for intermediate files
 (choose fast filesystem)
 --of -enable on the fly recording
 --help, -h -print this help and exit
EOT
 exit;
}
