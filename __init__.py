## BLOCK GPL
# 
#   __init__.py
#
#   Main file for addon recordmydesktop in linux.
#
# Copyright 2014 gumbi <pracacp@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
## END BLOCK GPL


bl_info = {
    "name": "Record My Desktop ",
    "author": "gumbicp",
    "version": (1, 0, 0),
    "blender": (2, 7, 5),
    "location": "",
    "description": "Linux Recording Addon",
    "warning": "For Linux olny, and required recordmydesktop installed !!!",
    "wiki_url": "",
    "category": "System"}


import bpy 
from bpy.types import Header, Menu


class INFO_MT_editormenu_RecorMyDesktop(Header):
    """
    Header in info window.
    Record my Desktop.
    """
    bl_space_type = 'INFO'
    bl_label = "INFO_MT_editormenu_RecorMyDesktop"
    
    def draw(self, context):
        layout = self.layout
            
        layout.menu("INFO_MT_recorMyDesktop")


class INFO_MT_recorMyDesktop(Menu):
    """
    Main Menu
    """
    bl_label = "RecMyDesktop"
    
    def draw(self, context):
        layout = self.layout 
        
        row = layout.row()
        row.label(text="Record")
        
        row = layout.row()      
        row.operator(Recorder.bl_idname, icon='FRAME_NEXT') 
             

class Recorder(bpy.types.Operator):
    """
    Operator. Sets options and start new term in new thread.
    
    """
    bl_idname = "recorder.rec"
    bl_label = "Rec"
    myfile_name = bpy.props.StringProperty(name="File name", default="tut")
    fps = bpy.props.IntProperty(name="FPS: ", default=60, min=15, max=120)
    freq = bpy.props.IntProperty(name="Freq Hz", default=22050, min=22050, max=44100)
    delay = bpy.props.IntProperty(name="Delay:", default=5, min=1, max=15)
    int_x = bpy.props.IntProperty(name="Screen Offset x: ", default=0, min=0, max=800)
    int_y = bpy.props.IntProperty(name="Screen Offset y: ", default=0, min=0, max=500)
    width = bpy.props.IntProperty(name="Screen Width", default=1060, min=100, max=1700)
    height = bpy.props.IntProperty(name="Screen Height", default=740, min=100, max=900)
    no_sound = bpy.props.BoolProperty(name="No Sound",description="Record Quiet")
    no_cursor = bpy.props.BoolProperty(name="No Cursor",description="Hide cursor")
    on_the_fly_encoding = bpy.props.BoolProperty(name="On the fly encoding")
    overwrite = bpy.props.BoolProperty(name="Overwrite",description="overwrite out file")
    devicestr = bpy.props.StringProperty(name="Device name", default="hw:0,0")
    devicebool = bpy.props.BoolProperty(name="Device on/off", description="On/Off device option")
    
    def execute(self, context):
        self.report({'INFO'}, "Start Recording")
        message = "Options Values: file name: {fname},fps: {FPS}, screen offset x: {offx}, screen offset y : {offy}, Screen Width: {swidth}, Screen Height: {sheight},No Sound: {ns}".format(
                                                                                                                                                                                                   fname=self.myfile_name,
                                                                                                                                                                                                   FPS=self.fps,
                                                                                                                                                                                                   offx=self.int_x,
                                                                                                                                                                                                   offy=self.int_y,
                                                                                                                                                                                                   swidth=self.width,
                                                                                                                                                                                                   sheight=self.height,
                                                                                                                                                                                                   ns=self.no_sound
                                                                                                                                                                                                   )
        print(message)
        self.report({'INFO'}, message)
        try:
            from . import newterm as nt
            optional_dict = {"--quiet": self.no_sound,"--no-curso": self.no_cursor,"--on-the-fly-encoding": self.on_the_fly_encoding,"--overwrite": self.overwrite, "--device": self.devicebool,"device_name": self.devicestr}
            # !!! ważne nie zmieniać pozycji w args [8] dla optional_dict !!!
            obj_newterm = nt.NewTerm('--x', self.int_x, '--y', self.int_y, '--width', self.width, '--height', self.height, optional_dict, '--freq', self.freq,'--fps', self.fps, '--delay' , self.delay, '--output', self.myfile_name)
            obj_newterm.start_thread()
        except AttributeError as e:
            print(e)
        except ValueError as e:
            print(e)
        
        return {'FINISHED'}
    
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)
    
    
### REGISTER CLASS END FUNCTIONS IN BLENDER API ##
def register():
    bpy.utils.register_module(__name__)
 
   
def unregister():
    bpy.utils.unregister_module(__name__)


if __name__ == "__main__":
    register()

