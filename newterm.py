import subprocess 
import os
import threading
from bpy.utils import script_paths


TERM_EMULATOR = ['/bin/bash', '/usr/bin/x-terminal-emulator', '/usr/bin/xterm', '/usr/bin/xvt']
#TERM_OPTIONS_SEND = '-e'
#TERM_OPTIONS_P = '--profile'
#TERM_OPTIONS_PN = 'rec'

#TODO: Dopisać obsługę terminali na innych systemach (linux mint jest)

for index in range(len(TERM_EMULATOR)):
	if os.access(TERM_EMULATOR[index], os.F_OK):
		global index_term_dist
		index_term_dist = index
		
	if TERM_EMULATOR[index] == '/bin/bash':
		global TERM_OPTIONS_SEND
		TERM_OPTIONS_SEND = '-c'
		break
		
	elif TERM_EMULATOR[index] == '/usr/bin/x-terminal-emulator':
		global TERM_OPTIONS_SEND
		TERM_OPTIONS_SEND = '-e'
		break


class NewTerm(object):
    """
    Klasa otwiera nowy terminal w nowym wątku.
    """
    
    def __init__(self, *args):
        """
        Ustawienia ścieżki i tablicy stringów (komenda do wysłania w terminalu)
        TODO: usunąć printy
        """
        print(20*"*", "Class NewTerm", 20*"*")
        self.tab_term_commands = [TERM_EMULATOR[index_term_dist], TERM_OPTIONS_SEND] 
        self.filepath_wrapper = None
        self.filepaths = script_paths(user_pref=True,check_all=True)

        for path in self.filepaths:
            path = os.path.join(path, "addons/recordmydesktop/record.py")
            print("DEBUG __init__:: W Petli ścierzek :: ->",path)
            if os.access(path, os.F_OK):    
                self.filepath_wrapper = os.path.join(".",path)
                self.tab_term_commands.append(self.filepath_wrapper)
                for v in args:
                    if isinstance(v, dict):
                        for key, val in v.items():
                            if val:
                                if key == 'device_name': 
                                    continue
                                elif key == '--device':
                                    self.tab_term_commands.append('--device')
                                    self.tab_term_commands.append(str(args[8]['device_name']))
                                else:    
                                    self.tab_term_commands.append(str(key))
                    else:
                        self.tab_term_commands.append(str(v))
            
        print("DEBUG __init__:: ->Typ opcji:: ", type(args))
        print("DEBUG __init__:: ", args)
        print("DEBUG __init__:: Tablica zwrócona z funkcji bpy.utils.script_paths() :: ->", self.filepaths)
        print("DEBUG __init__:: Znaleziona ścieżka do pliku recordwrapper.py :: ->", self.filepath_wrapper)
        print("DEBUG __init__:: tab_term_commands :: ->", self.tab_term_commands)

    def new_term(self):
        """
        Przez moduł supbprocess wysłana komenda do systemu.
        

        """
        proc = subprocess.Popen(self.tab_term_commands, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        stdout_value = proc.communicate(b'/t return new term')[0]
        print("DEBUG new_term :: proc :: ", proc)

    def start_thread(self):
        """
        Metoda startuje nowy wątek jeżeli ścieżka w systemie jest prawdziwa.
        """
        try:
            if os.access(self.filepath_wrapper, os.F_OK):
                """
                    thr.exit() nie posiada takiej metody jak exit(). To moja sztuczka do uziemienia wątku tak aby 
                 przy ponownym wywołaniu pliku wystartował nowy wątek .
                 Na dziś nie znalazłem dobrego sposobu(czystego), innego niż beszczelne wygenerowanie
                 błędu który przechwycony w __init__.py pasujemy (pass).
                 try:
                     from . import newterm as term
                 except AttributeError:
                     pass
                 Bez tego wybiegu blender nie wystartował by nowej konsoli o tej samej nazwie (profilu)
                """
                print("... start wątku ... ")
                thr = threading.Thread(target=self.new_term, name="terminal")
                thr.start()
                thr.exit()
            else:
                raise ValueError("Path ERROR :"+self.filepath_wrapper)
        except TypeError:
            print("Error :: -> Blender nie znalazł ścieżki do pliku recordwrapper.py. Sprawdz poprawność lokalizacji wtyczki !!! ", script_paths(user_pref=True,check_all=True) , " W tych lokalizacjach powinna się ona znajdować w folderze addons/recordmydesktop/record.py")

